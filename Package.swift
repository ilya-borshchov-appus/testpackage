// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    
    name: "TestPackage",
    defaultLocalization: "en",
    products: [
        
        .library(
            
            name: "TestPackage",
            targets: ["TestPackage"]),
    ],
    dependencies: [
        
    ],
    targets: [
        
        .target(
            
            name: "TestPackage",
            dependencies: [],
            path: "TestPackage",
            exclude: [],
            publicHeadersPath: "."
        ),
    ]
)
