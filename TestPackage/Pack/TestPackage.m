//
//  TestPackage.m
//  TestPackage
//
//  Created by Ilya on 29.03.2022.
//

#import "TestPackage.h"

@implementation TestPackage



@end

@implementation NSArray (Dick)

- (NSArray *)arrayByRemovingObjectttt:(id)object {

    NSArray *result;
    if ([self containsObject:object]) {
        NSMutableArray *mutableArray = [self mutableCopy];
        [mutableArray removeObject:object];
        result = [mutableArray copy];
    } else {
        result = self;
    }
    
    return result;
}

@end
